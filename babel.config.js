module.exports = function(api) {
  api.cache(true);
  return {
    presets: ['babel-preset-expo'],
    plugins: [
      [
				'module-resolver',
				{
					extensions: ['.js', '.jsx', '.ts', '.tsx', '.android.js', '.android.tsx', '.ios.js', '.ios.tsx'],
					root: ['./src'],
					alias: {
						'@atoms': './src/components/atoms',
						'@molecules': './src/components/molecules',
						'@organisms': './src/components/organisms',
						'@pages': './src/components/pages',
						'@templates': './src/components/templates',
						'@assets': './src/assets',
						'@containers': './src/containers',
						'@navigation': './src/navigation',
						'@styles': './src/styles',
						'@tests': './src/tests',
						'@utils': './src/utils',
						'@hooks': './src/hooks',
						'@state': './src/state',
						'@src': './src'
					}
				}
			],
			'react-native-reanimated/plugin',
    ]
  };
};
