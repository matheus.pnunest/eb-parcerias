import { useFontsLoading } from '@hooks'
import { GestureHandlerRootView } from 'react-native-gesture-handler'
import { Navigation } from '@navigation'
import { RecoilRoot } from 'recoil'

const App = () => {
  const fontsLoaded = useFontsLoading()
  return (
    <>
      {fontsLoaded && (
        <GestureHandlerRootView style={{ flex: 1 }}>
          <RecoilRoot>
            <Navigation />
          </RecoilRoot>
        </GestureHandlerRootView>
      )}
    </>
  )
}

export default App
