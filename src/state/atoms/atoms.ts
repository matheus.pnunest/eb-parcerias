import { atom } from 'recoil'

export type NotificationList = {
  id: number
  title: string
  date: string
  description: string
  read: boolean
}[]

export interface LocationData {
  coords: {
    latitude: number
    longitude: number
  }
  city: string | null
  region: string | null
}

export interface LocationError {
  message: string
}

export const notificationsState = atom<NotificationList>({
  key: 'notificationsState',
  default: [],
})

export const locationState = atom<LocationData | null>({
  key: 'locationState',
  default: null,
})
