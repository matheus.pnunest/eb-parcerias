export type ColorType = typeof colors
export const colors = {
  // B&W
  black: '#121212',
  white:'#fff',

  gray: '#F8F8F8',
  gray0: '#F0F0F0',
  gray1: '#F6F6F6',
  gray2: '#E8E8E8',
  gray3: '#D1D5DB',
  gray4: '#666666',
  gray5: '#5B5B5B',

  card: '#F1F1F1',
  border: '#A1A5AC',
  placeholder: '#BDBDBD',

  // Colors
  green: '#5DB075',
  green2: '#0F8955',
  red: '#E56250',
  red2: '#F63030',
  yellow: '#FBFF2F',
  gold: '#FFB84E',
  blue: '#3038F6',

  // Transparencies
  glass1: 'rgba(255, 255, 255, 0.05)',
  glass2: 'rgba(255, 255, 255, 0.12)',
  glass3: 'rgba(255, 255, 255, 0.38)',

  transparent: 'transparent',
}
