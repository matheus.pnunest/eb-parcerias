export type ShadowsType = 0 | 1
import { colors } from './colors'

export const shadows = {
	0: null,
	1: {
    elevation: 10,
    shadowColor: colors.black,
    shadowOffset: {
      width: 0,
      height: 0
    },
    shadowOpacity: 0.12,
    shadowRadius: 12,
  }
}
