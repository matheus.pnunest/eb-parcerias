const sizeArray = [12, 14, 16, 20, 24, 30] as const

export type FontSize = (typeof sizeArray)[number]

export type FontWeight = keyof typeof fontWeight
const fontWeight = {
  300: 'InterLight', // 300 - Light
  400: 'InterMedium', // 400 - Regular
  500: 'InterRegular', // 500 - Medium
  600: 'InterSemiBold', // 600 - SemiBold
}
