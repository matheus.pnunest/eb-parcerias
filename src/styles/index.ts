import { colors } from './colors'
import type { ColorType } from './colors'

import { icon } from './icons'
import type { IconType } from './icons'

import { shadows } from './shadows'
import type { ShadowsType } from './shadows'

import type { FontWeight, FontSize } from './fonts'

import {
  os,
  screenHeight,
  screenWidth,
  deviceHeight,
  deviceWidth,
  statusBar,
  statusBarHeight,
  navigationBarHeight,
  px,
} from './dimensions'

export {
  os,
  screenHeight,
  screenWidth,
  deviceHeight,
  deviceWidth,
  statusBar,
  statusBarHeight,
  navigationBarHeight,
  icon,
  colors,
  shadows,
  px,
}

export type {
  ColorType,
  IconType,
  FontSize,
  FontWeight,
  ShadowsType,
}
