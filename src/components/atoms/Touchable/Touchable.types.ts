import { AccessibilityRole } from 'react-native'
import { ViewType } from '../View/View.types'

export interface TouchableProps extends ViewType {
  opacity: number
  onPress: () => void
  disabled?: boolean
  accessibilityRole?: AccessibilityRole
  chldren?: React.ReactNode
}
