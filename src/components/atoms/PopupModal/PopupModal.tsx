import { useEffect } from 'react'
import {
	TouchableWithoutFeedback,
	Keyboard,
	Modal,
} from 'react-native'
import { View } from '../View/View'
import Animated, {
	useAnimatedStyle,
	useSharedValue,
	withTiming,
} from 'react-native-reanimated'
import { colors } from '@styles'
import { PopupModalProps } from './PopupModal.types'

export const PopupModal: React.FC<PopupModalProps> = ({ children, onDismiss, isVisible }) => {
	const opacity = useSharedValue(0)

	const backdropAnimatedStyle = useAnimatedStyle(() => ({
		opacity: opacity.value * 0.3,
	}))

	useEffect(() => {
		opacity.value = withTiming(isVisible ? 1 : 0)
		if (!isVisible) Keyboard.dismiss()
	}, [isVisible])

	if (!isVisible) return null

	return (
		<Modal transparent visible={isVisible}>
			<View flex1 main='center' cross='center'>
				<TouchableWithoutFeedback onPress={onDismiss}>
					<Animated.View 
						style={[ 
							{
								position: 'absolute',
								top: 0,
								left: 0,
								bottom: 0,
								right: 0,
								backgroundColor: colors.black,
							}, 
							backdropAnimatedStyle
						]} 
					/>
				</TouchableWithoutFeedback>
				{children}
			</View>
		</Modal>
	)
}
