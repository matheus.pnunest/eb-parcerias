import { StyleProp, ViewStyle } from 'react-native'
import { IconType, ColorType } from '@styles'

export interface IconProps {
  name: IconType
  color: keyof ColorType
  size: number
  style?: StyleProp<ViewStyle>
  stroke?: string
  outlined?: boolean
}
