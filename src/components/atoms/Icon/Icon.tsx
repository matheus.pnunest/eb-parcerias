import { icon, colors } from '@styles'
import { Svg, Path } from 'react-native-svg'
import { IconProps } from './Icon.types'

export const Icon: React.FC<Partial<IconProps>> = ({
  name = 'dorbell',
  color = 'black',
  size = 30,
  stroke,
  style,
  outlined,
}) => {
  // First element is the viewBox, the rest are paths.
  const [viewBox, ...paths] = icon[name]
  return (
    <Svg width={size} height={size} viewBox={viewBox} fill='none' style={{ ...(style as object) }}>
      {paths.map((d: string) => (
        <Path
          key={d}
          fillRule='evenodd'
          clipRule='evenodd'
          d={d}
          fill={outlined ? 'none' : colors[color]}
          stroke={outlined ? colors[color] : 'none'}
          strokeWidth={outlined ? 1 : 0} 
        />
      ))}
    </Svg>
  )
}
