import { ViewType } from '../View/View.types'

export interface ScrollProps extends ViewType {
  hideIndicator?: boolean
  horizontal?: boolean
}
