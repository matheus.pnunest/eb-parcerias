import { Icon } from './Icon/Icon'
import { PopupModal } from './PopupModal/PopupModal'
import { SafeArea } from './SafeArea/SafeArea'
import { Scroll } from './Scroll/Scroll'
import { Text } from './Text/Text'
import { Touchable } from './Touchable/Touchable'
import { View } from './View/View'

export { 
  Icon,
  PopupModal,
  SafeArea,
  Scroll,
  Text,
  Touchable,
  View,
}
