import { DimensionValue, Text as TextNative } from 'react-native'
import { colors } from '@styles'
import { TextComponentProps } from './Text.types'

export const Text: React.FC<TextComponentProps> = ({
  children,
  weight = 500,
  color = 'black',
  width,
  center,
  mb,
  mt,
  ml,
  size = 14,
  flex1,
  style,
}) => (
  <TextNative
    style={[
      {
        fontWeight: `${weight}`,
        textAlign: center ? 'center' : 'left',
        color: colors[color],
        width: width as DimensionValue,
        fontSize: size,
        marginBottom: mb,
        marginTop: mt,
        marginLeft: ml,
        alignItems: 'center',
        flex: flex1 ? 1 : undefined,
      },
      style,
    ]}
  >
    {children}
  </TextNative>
)

