import { StyleProp, TextStyle } from 'react-native'
import { FontWeight, FontSize, ColorType } from '@styles'

interface TextComponentProps {
  children: React.ReactNode
  weight?: FontWeight
  size?: FontSize
  color?: keyof ColorType
  width?: number | string
  center?: boolean
  mb?: number
  mt?: number
  ml?: number
  flex1?: boolean
  style?: StyleProp<TextStyle>
}

export type { TextComponentProps }
