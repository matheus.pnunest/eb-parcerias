import { View, Scroll } from '@atoms'
import { MainProps } from './Main.types'
import { TemplateMainLogoBox } from '@molecules'

export const Main: React.FC<MainProps> = ({ 
  children,
  view = false,
  type: Container = view ? View : Scroll,
  noPadding = false,
}) => (
  <Container flex1 h='100%' bgc='white' hideIndicator>
    <TemplateMainLogoBox />
    <View 
      flex1 
      ph={noPadding ? 0 : 24}
      pt={24}
    >
      {children}
    </View>
  </Container>
)
