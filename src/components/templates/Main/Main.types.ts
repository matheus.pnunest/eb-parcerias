import { View, Scroll } from '@atoms'

export interface MainProps {
  children?: React.ReactNode
  view?: boolean
  type?: typeof Scroll | typeof View
  noPadding?: boolean
}
