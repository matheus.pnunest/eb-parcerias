import { IconType } from '@styles'

export interface NotificationTagProps {
  icon: IconType
  active: boolean
  count: number
}
