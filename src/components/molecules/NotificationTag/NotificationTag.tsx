import { View, Text, Icon } from '@atoms'
import { NotificationTagProps } from './NotificationTag.types'

export const NotificationTag: React.FC<Partial<NotificationTagProps>> = ({ icon, active, count = 0 }) => (
  <View cross='flex-end'>
    <Icon name={icon} color="black" />
    {active && count > 0 && (
      <View 
        round={17} 
        bgc='red2' 
        absolute
        main='center'
        cross='center'
        style={{ top: -2, right: -5 }}
      >
        <Text size={12} color='white'>{count}</Text>
      </View>
    )}
  </View>
)
