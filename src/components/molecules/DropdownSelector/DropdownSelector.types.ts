export interface DropdownSelectorProps {
  title: string
  onSelect?: () => void
  isOpen?: boolean
  disabled?: boolean
}
