import { Text, Touchable, Icon } from '@atoms'
import { DropdownSelectorProps } from './DropdownSelector.types'

export const DropdownSelector: React.FC<DropdownSelectorProps> = ({ 
  title, 
  onSelect, 
  isOpen, 
  disabled,
}) => (
  <Touchable 
    bw={1}
    bgc='gray1'
    bc='gray2'
    h={50}
    row
    main='space-between'
    cross='center'
    ph={12}
    onPress={onSelect}
    br={4}
    disabled={disabled}
  > 
    <Text color='placeholder' size={16}>
      {title}
    </Text>
    <Icon 
      size={14} 
      name={isOpen ? 'arrow_up' : 'arrow_down'}
    />
  </Touchable>
)
