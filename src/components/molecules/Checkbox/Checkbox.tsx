import { useState } from 'react'
import { Icon, View, Touchable } from '@atoms'

export const Checkbox = () => {
  const [isChecked, setIsChecked] = useState(false)
  const toggleCheckbox = () => setIsChecked(!isChecked)
  return (
    <View row cross='center'>
      <Touchable 
        onPress={toggleCheckbox} 
        w={16}
        h={16}
        bgc='green'
        bw={1}
        br={4}
        bc='green2'
        main='center'
        cross='center'
      >
        {isChecked && <Icon name='check' size={8} color='white' />}
      </Touchable>
    </View>
  )
}
