import { ImageSourcePropType } from 'react-native'
export interface HomeListCardProps {
  title: string
  image: ImageSourcePropType
  horizontal?: boolean
  last?: boolean
  segment?: string
  benefit?: string
  expiration?: string
  address?: string
  rating?: string
}
