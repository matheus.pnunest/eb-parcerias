import { Image } from 'react-native'
import { View, Text, Icon, Touchable } from '@atoms'
import { useNavigation } from '@hooks'
import { px } from '@styles'
import { HomeListCardProps } from './HomeListCard.types'

export const HomeListCard: React.FC<HomeListCardProps> = ({ 
  title, 
  image, 
  horizontal, 
  last, 
  segment,
  benefit,
  expiration,
  address,
  rating,
}) => {
  const navigation = useNavigation()
  return (
    <Touchable onPress={() => navigation.navigate('ParceriasDetail')}>
      <View mb={horizontal ? 0 : 12} row={horizontal}>
        <View 
          w={px(110)} 
          h={px(110)} 
          br={px(8)} 
          bgc='gray1' 
          mb={px(8)}
          main='center'
          cross='center'
        >
          <Image source={image} />
        </View>
        {horizontal && <View mh={4} />}
        <View flex1>
          <View row main='space-between'>
            <Text>{title}</Text>
            {horizontal && rating && (
              <View row>
                <Icon  size={16} name='star1' color='gold' />
                <Text ml={4} weight={400}>{rating}</Text>
              </View>
            )}
          </View>
          {horizontal && (
            <>
              {segment && <Text mt={8}>{segment}</Text>}
              {benefit && <Text mt={8} color='red'>{benefit}</Text>}
              {expiration && <Text color='red'>Validade: {expiration}</Text>}
              {address && <Text mt={8} size={12}>{address}</Text>}
            </>
          )}
        </View>
      </View>
      {horizontal && !last && (
        <View h={1} bgc='border' mv={12} />
      )}
    </Touchable>
  )
}
