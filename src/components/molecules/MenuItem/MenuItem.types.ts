import { IconType } from '@styles'

export interface MenuItemProps {
  name: string
  icon: IconType
  onPress?: () => void
}
