import { View, Text, Icon, Touchable } from '@atoms'
import { MenuItemProps } from './MenuItem.types'

export const MenuItem: React.FC<MenuItemProps> = ({ name, icon, onPress }) => (
  <>
    <Touchable
      row
      main='space-between'
      w='100%'
      onPress={onPress}
    >
      <View row cross='center'>
        <Icon name={icon} />
        <View mh={8} />
        <Text color='black' size={16}>
          {name}
        </Text>
      </View>
      <Icon name='arrow_forward' size={20} />
    </Touchable>
    <View mv={16} />
  </>
)
