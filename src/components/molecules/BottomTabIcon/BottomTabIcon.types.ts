import { IconType } from '@styles'

export interface BottomTabIconProps {
  title: string
  icon: IconType
  active: boolean
}
