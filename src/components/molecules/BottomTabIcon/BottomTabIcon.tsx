import { View, Text } from '@atoms'
import { BottomTabIconProps } from './BottomTabIcon.types'
import { NotificationTag } from '../NotificationTag/NotificationTag'
import { useNotifications } from '@hooks'

export const BottomTabIcon: React.FC<Partial<BottomTabIconProps>> = ({ title, icon, active }) => {
  const { notifications } = useNotifications()
  const unseenNotifications = notifications.filter(notification => !notification.read)
  const notificationCount = unseenNotifications.length
  return (
    <View cross='center'>
      <NotificationTag 
        icon={icon} 
        active={icon === 'dorbell'} 
        count={notificationCount}
      />
      <View mb={6} />
      <Text color={active ? 'green2' : 'black'} size={12}>
        {title}
      </Text>
    </View>
  )
}
