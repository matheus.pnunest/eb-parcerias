import { Touchable, Icon } from '@atoms'
import { useNavigation } from '@hooks'

export const CancelButton = () => {
  const navigation = useNavigation()
  return (
    <Touchable 
      onPress={() => navigation.goBack()}
      absolute
      rx={8}
      y={8}
    >
      <Icon size={25} name="cancel" />
    </Touchable>
  )
}
