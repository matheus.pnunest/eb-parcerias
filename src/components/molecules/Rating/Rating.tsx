import { useState } from 'react'
import { View, Touchable, Icon } from '@atoms'

export const Rating = () => {
  const [rating, setRating] = useState(0)

  const handleStarPress = (idx: number) => {
    if (idx === rating - 1) setRating(0)
    else setRating(idx + 1)
  }

  return (
    <View row>
      {[...Array(5).keys()].map((idx) => (
        <Touchable
          key={idx}
          onPress={() => handleStarPress(idx)}
        >
          <Icon 
            size={32}
            name='star1'
            {...(idx < rating && { color: 'gold' })}
            outlined={idx >= rating}
          />
        </Touchable>
      ))}
    </View>
  )
}
