import { ColorType } from '@styles'
import { DimensionValue } from 'react-native'

export interface ButtonProps {
  w: DimensionValue
	onPress: () => void
  type: 'primary' | 'secondary'
  disabled: boolean
  color: keyof ColorType
  title: string
}
