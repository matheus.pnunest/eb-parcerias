import { Text, Touchable } from '@atoms'
import { ButtonProps } from './Button.types'

export const Button: React.FC<Partial<ButtonProps>> = ({ 
  w, 
  onPress, 
  type = 'primary', 
  color = 'green',
  title,
  disabled,
}) => (
  <Touchable 
    ph={!w ? 32 : 0} 
    h={51}
    bgc={disabled ? 'gray1' : type === 'primary' ? color : 'gray1'}
    br={32} 
    w={!w ? '100%' : w}
    main='center'
    cross='center'
    onPress={onPress}
    disabled={disabled}
  >
    <Text 
      weight={600} 
      size={16}
      color={disabled ? 'gray3' : type === 'primary' ? 'white' : color}
    >
      {title}
    </Text>
  </Touchable>
)
