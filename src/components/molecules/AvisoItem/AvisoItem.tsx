import { View, Text, Touchable } from '@atoms'
import { colors } from '@styles'
import { AvisoItemProps } from './AvisoItem.types'

export const AvisoItem: React.FC<AvisoItemProps> = ({ onPress, title, date, description }) => (
  <>
    <Touchable w='100%' row onPress={onPress}>
      <View w={50} h={50} br={8} bgc='gray1' mr={12} />
      <View 
        flex1 
        pb={16}
        style={{
          borderBottomWidth: 1,
          borderBottomColor: colors.gray3,
        }}
      >
        <View row main='space-between' w='100%' mb={8}>
          <Text weight={600} size={16}>{title}</Text>
          <Text color='placeholder'>{date}</Text>
        </View>
        <Text>{description}</Text>
      </View>
    </Touchable>
    <View mv={8} />
  </>
)
