export interface AvisoItemProps {
  onPress?: () => void
  title: string
  date: string
  description: string
}
