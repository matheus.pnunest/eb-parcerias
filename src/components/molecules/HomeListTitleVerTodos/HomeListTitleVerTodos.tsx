import { View, Text, Touchable } from '@atoms'
import { useNavigation } from '@hooks'
import { HomeListTitleVerTodosProps } from './HomeListTitleVerTodos.types'

export const HomeListTitleVerTodos: React.FC<HomeListTitleVerTodosProps> = ({ title, navigate }) => {
  const navigation = useNavigation()
  return (
    <View row main='space-between' cross='center' mb={12}>
      <Text size={24}>{title}</Text>
      <Touchable onPress={() => navigation.navigate(navigate)}>
        <Text weight={400}>Ver todos</Text>
      </Touchable>
    </View>
  )
}
