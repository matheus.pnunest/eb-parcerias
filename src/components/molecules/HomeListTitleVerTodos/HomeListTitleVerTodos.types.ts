import { RoutesList } from '@src/navigation/routes/Routes.types'

export interface HomeListTitleVerTodosProps {
  title: string
  navigate: keyof RoutesList
}
