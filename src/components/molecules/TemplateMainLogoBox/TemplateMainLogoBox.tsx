import { View } from '@atoms'
import { statusBarHeight } from '@styles'
import { Image } from 'react-native'

export const TemplateMainLogoBox = () => (
  <View 
    h={128 + statusBarHeight} 
    bgc='gray'
    main='flex-end'
  >
    <View 
      w='100%' 
      h={128}
      main='center'
      cross='center'
    >
      <Image 
        source={require('@assets/images/logo.png')} 
        style={{ 
          width: 180,
          resizeMode: 'contain',
        }}
      />
    </View>
  </View>
)
