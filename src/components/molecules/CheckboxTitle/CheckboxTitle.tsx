import { View, Text } from '@atoms'
import { Checkbox } from '../Checkbox/Checkbox'
import { CheckboxTitleProps } from './CheckboxTitle.types'

export const CheckboxTitle: React.FC<CheckboxTitleProps> = ({ title }) => (
  <View row main='space-between'>
    <Text size={12} color='gray4'>{title}</Text>
    <View mh={4} />
    <Checkbox />
  </View>
)
