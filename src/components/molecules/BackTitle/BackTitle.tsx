import { View, Text, Icon, Touchable } from '@atoms'
import { useNavigation } from '@hooks'
import { BackTitleProps } from './BackTitle.types'

export const BackTitle: React.FC<BackTitleProps> = ({ title }) => {
  const navigation = useNavigation()
  return (
    <Touchable row cross='center' onPress={() => navigation.goBack()}>
      <Icon size={25} name='arrow_back2' />
      <View mh={4} />
      <Text size={24}>{title}</Text>
    </Touchable>
  )
}
