export interface DeleteModalProps {
  onDismiss?: () => void
}
