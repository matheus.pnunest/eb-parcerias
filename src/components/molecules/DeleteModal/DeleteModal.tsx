import { View, Text } from '@atoms'
import { Button } from '../Button/Button'
import { DeleteModalProps } from './DeleteModal.types'

export const DeleteModal: React.FC<DeleteModalProps> = ({ onDismiss }) => (
  <View 
    w={365} 
    h={540}
    br={32}
    bgc='gray2' 
    main='center'
    cross='center'
    p={24}
    s={1}
  >
    <Text size={24} weight={300} center>
      Deseja realmente excluir o serviço ativo?
    </Text>
    <View mv={24} />
    <Button 
      title='Sim'
      onPress={() => null}
      color='red'
      w={158}
    />
    <View mv={12} />
    <Button 
      title='Não'
      onPress={onDismiss}
      color='gray5'
      w={158}
    />
  </View>
)
