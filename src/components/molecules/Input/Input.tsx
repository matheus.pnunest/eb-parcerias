import { View, Text } from '@atoms'
import { TextInput, TouchableWithoutFeedback } from 'react-native'
import React, { useState, useRef, useEffect, forwardRef, useImperativeHandle } from 'react'
import { InputProps, InputRef } from './Input.types'
import { colors } from '@styles'
import { Touchable } from '../../atoms/Touchable/Touchable'

const HEIGHT = 50
const HEIGHT_SMALL = 36

const Input: React.ForwardRefRenderFunction<InputRef, InputProps> = (
	{
		// label,
		isPassword,
		onChangeText,
		isFocused,
		onBlur,
		onFocus,
		onTogglePassword,
		togglePassword,
		placeholder,
		onSubmit,
		multiline,
		error,
		small,
		value = '',
		onSelectionChange,
		style,
		extraText,
		h = HEIGHT,
		...rest
	},
	ref
) => {
	const [isFocusedState, setIsFocused] = useState(false)
	const [secureText, setSecureText] = useState(true)
	const inputRef = useRef<any>(null)

	const setFocus = () => inputRef.current?.focus()
	const setBlur = () => inputRef.current?.blur()

	const toggleVisibility = (toggle?: boolean) => {
		if (toggle === undefined) {
			if (onTogglePassword) {
				onTogglePassword(!secureText)
			}
			setSecureText(!secureText)
			if (secureText) setFocus()
			else setBlur()
		} else if (!((secureText && !toggle) || (!secureText && toggle))) {
			if (onTogglePassword) {
				onTogglePassword(!toggle)
			}
			setSecureText(!toggle)
			if (toggle) setFocus()
			else setBlur()
		}
	}

	useEffect(() => {
		if (isFocused === undefined) {
			if (value !== '' || isFocusedState) {
				setIsFocused(true)
			} else if (value === '' || value === null) {
				setIsFocused(false)
			}
		}
	}, [value])

	useEffect(() => {
		if (isFocused !== undefined) {
			if (value !== '' || isFocused) {
				setIsFocused(true)
			} else {
				setIsFocused(false)
			}
		}
	}, [isFocused, value])

	useEffect(() => {
		if (togglePassword !== undefined) {
			toggleVisibility(togglePassword)
		}
	}, [togglePassword])

	useImperativeHandle(ref, () => ({
		focus() {
			inputRef.current.focus()
		},
		blur() {
			inputRef.current.blur()
		}
	}))

	const handleFocus = () => setIsFocused(true)

	const handleBlur = () => {
		if (value === '') setIsFocused(false)
	}

	const onSubmitEditing = () => {
		if (onSubmit !== undefined) onSubmit()
	}

	const onChangeTextCallback = (val: string) => (onChangeText ? onChangeText(val) : false)

	return (
		<TouchableWithoutFeedback onPress={setFocus}>
			<View row style={{ ...(style as object) }}>
				<View
          bc={error ? 'red' : isFocusedState ? 'green' : 'gray2'}
          bgc='gray1'
          bw={1}
          br={4}
          ph={24}
          main='center'
          cross='center'
          h={h}
          flex1
				>
					<View flex1 row cross='center'>
						<TextInput
							onSubmitEditing={onSubmitEditing}
							secureTextEntry={isPassword !== undefined ? isPassword && secureText : false}
							onFocus={onFocus !== undefined ? onFocus : handleFocus}
							onBlur={onBlur !== undefined ? onBlur : handleBlur}
							ref={inputRef}
							onChangeText={onChangeTextCallback}
              style={{
                ...(!small && { minHeight: h - 16 }),
                flex: 1,
                color: colors.black,
                paddingBottom: 8,
                paddingTop: small ? 0 : HEIGHT_SMALL / 2 - 8,
								fontSize: 16
              }}
							selectionColor={colors.green}
							placeholder={placeholder}
							placeholderTextColor={colors.placeholder}
							{...{ value, multiline, ...rest }}
						/>
						{!!extraText && <Text>{extraText}</Text>}

            {error && <Text style={{ marginRight: 8 }}>{error}</Text>}

						{isPassword && (
							<Touchable 
                main='center'
                cross='center'
                onPress={() => toggleVisibility()}
              >
								{secureText ? (
                  <Text size={16} color='green'>Mostrar</Text>
                ) : (
                  <Text size={16} color='green'>Ocultar</Text>
                )}
							</Touchable>
						)}
					</View>
				</View>
			</View>
		</TouchableWithoutFeedback>
	)
}

export default forwardRef(Input)
