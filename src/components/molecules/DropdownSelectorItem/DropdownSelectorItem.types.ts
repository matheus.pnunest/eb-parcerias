export interface DropdownSelectorItemProps {
  title: string
  onSelect: () => void
  selected?: boolean
}
