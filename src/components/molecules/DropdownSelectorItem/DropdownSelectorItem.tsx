import { Text, Touchable } from '@atoms'
import { DropdownSelectorItemProps } from './DropdownSelectorItem.types'

export const DropdownSelectorItem: React.FC<DropdownSelectorItemProps> = ({ title, selected, onSelect }) => (
  <Touchable 
    h={50} 
    ph={24} 
    main='center'
    bgc={selected ? 'gray' : 'gray3'}
    onPress={onSelect}
  >
    <Text>{title}</Text>
  </Touchable>
)
