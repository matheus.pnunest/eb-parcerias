import { View, Text, Icon } from '@atoms'
import { useLocationPermission } from '@hooks'

export const LocationTitle = () => {
  const { location } = useLocationPermission()
  return (
    <View row cross='center'>
      <Icon name='location' size={25} />
      <View mh={4} />
      {location?.city && location?.region ? (
        <Text size={16}>{location.city} - {location.region}</Text>
      ) : (
        <Text size={16}>Loading...</Text>
      )}
    </View>
  )
}
