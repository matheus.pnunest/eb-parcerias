export interface BottomSheetProps {
  children: React.ReactNode
  onClose: () => void
  isVisible: boolean
}
