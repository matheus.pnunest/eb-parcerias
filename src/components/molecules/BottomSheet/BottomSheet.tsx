import 'react-native-gesture-handler'
import { PopupModal, View, Text, Touchable } from '@atoms'
import { deviceHeight } from '@styles'
import {
  Gesture,
  GestureDetector,
} from 'react-native-gesture-handler'
import Animated, {
  useAnimatedStyle,
  useSharedValue,
  withSpring,
  runOnJS,
  withTiming,
  SlideInDown,
  SlideOutDown,
} from 'react-native-reanimated'
import { BottomSheetProps } from './BottomSheet.types'

export const HEIGHT = deviceHeight / 2
export const OVERDRAG = 20

export const BottomSheet: React.FC<BottomSheetProps> = ({ children, onClose, isVisible }) => {
  const offset = useSharedValue(0)

  const toggleSheet = () => {
    onClose()
    offset.value = 0
  }

  const pan = Gesture.Pan()
    .onChange((event) => {
      const offsetDelta = event.changeY + offset.value
      const clamp = Math.max(-OVERDRAG, offsetDelta)
      offset.value = offsetDelta > 0 ? offsetDelta : withSpring(clamp)
    })
    .onFinalize(() => {
      if (offset.value < HEIGHT / 3) {
        offset.value = withSpring(0)
      } else {
        offset.value = withTiming(HEIGHT, {}, () => {
          runOnJS(toggleSheet)()
        })
      }
    })

  const translateY = useAnimatedStyle(() => ({
    transform: [{ translateY: offset.value }],
  }))

  return (
    <PopupModal
      isVisible={isVisible}
      onDismiss={toggleSheet}
    >
      <GestureDetector gesture={pan}>
        <Animated.View 
          entering={SlideInDown.springify().damping(15)}
          exiting={SlideOutDown}
          style={[
            {
              maxHeight: HEIGHT,
              width: '100%',
              position: 'absolute',
              bottom: -OVERDRAG * 1.1,
              zIndex: 1,
            },
            translateY,
          ]}
        >
          <View bgc='gray3'>
            <View 
              h={42} 
              ph={16} 
              row 
              main='flex-end' 
              cross='center'
              bgc='gray'
            >
              <Touchable onPress={toggleSheet}>
                <Text size={24}>OK</Text>
              </Touchable>
            </View>
            <View pv={16} mb={140}>
              {children}
            </View>
          </View>
        </Animated.View>
      </GestureDetector>
    </PopupModal>
  )
}
