import { Touchable, Text } from '@atoms'
import { AvalieTagProps } from './AvalieTag.types'

export const AvalieTag: React.FC<AvalieTagProps> = ({ tag }) => (
  <Touchable
    pv={8}
    ph={12}
    br={24}
    mh={8}
    main='center'
    cross='center'
    bw={1}
  >
    <Text size={12}>
      {tag}
    </Text>
  </Touchable>
)
