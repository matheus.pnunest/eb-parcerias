import { View } from '@atoms'
import { LocationTitle, BackTitle, HomeListCard } from '@molecules'
import { Main } from '@templates'
import { SegmentosProps } from './Segmentos.types'

const segmentosList = [
  { title: 'Educação', image: require('@assets/images/22.png') },
  { title: 'Saúde', image: require('@assets/images/23.png') },
  { title: 'Serviços', image: require('@assets/images/24.png') },
]

export const Segmentos: React.FC<SegmentosProps> = () => (
  <Main>
    <LocationTitle />
    <View mv={8} />
    <BackTitle title='Segmentos' />
    <View mv={8} />

    {segmentosList.map(({ title, image }, idx) => (
      <HomeListCard
        key={idx}
        title={title}
        image={image}
        horizontal
        last={idx === segmentosList.length - 1}
      />
    ))}

  </Main>
)
