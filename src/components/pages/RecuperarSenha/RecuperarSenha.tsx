import { useState } from 'react'
import { View, Text } from '@atoms'
import { Input, Button } from '@molecules'
import { Main } from '@templates'
import { useNavigation } from '@hooks'

export const RecuperarSenha = () => {
  const [cnpj, setCNPJ] = useState('')
  const navigation = useNavigation()
  return (
    <Main>
      <View cross='center'>
        <Text size={16}>Recuperação de Senha</Text>

        <View mv={8} />

        <Text 
          size={14} 
          weight={300} 
          center 
          width={220}
        >
          Para recuperar a sua senha, informe o CNPJ cadastrado. 
          Enviaremos um link por e-mail para a alteração da senha.
        </Text>

        <View mv={12} />

        <Input 
          label='CNPJ'
          placeholder='99.999.999/0001-99' 
          small
          value={cnpj}
          onChangeText={setCNPJ}
        />

        <View mv={32} />

        <Button
          title='Entrar'
          onPress={() => {}}
          w={92}
        />
      </View>
    </Main>
  )
}
