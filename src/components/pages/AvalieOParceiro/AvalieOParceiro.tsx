import { useState } from 'react'
import { FlatList } from 'react-native'
import { View, Text } from '@atoms'
import { Button, CancelButton, Input, Rating, AvalieTag } from '@molecules'

const tagList = [
  'Benefício atraente',
  'Ótima localização',
  'Facilidade no uso',
  'Bom atendimento',
]

export const AvalieOParceiro = () => {
  const [value, setValue] = useState('')
  return (
    <View flex1>
      <View h={34} mb={28}>
        <View
          row 
          absolute 
          w='100%' 
          h={34}
          main='center'
          cross='center'
        >
          <Text weight={600} size={16}>
            Avaliação
          </Text>
        </View>
        <CancelButton />
      </View>

      <View ph={24}>
        <Text size={24} mb={8}>
          Avalie o Parceiro
        </Text>

        <Rating />

        <View h={24} />

        <Text size={16} mb={24}>
          Do que mais gostou?
        </Text>

        <FlatList
          data={tagList}
          renderItem={({ item }) => <AvalieTag tag={item} />}
          keyExtractor={item => item}
          numColumns={2}
          ItemSeparatorComponent={() => <View mv={8} />}
        />

        <View h={24} />

        <Text size={16} mb={8}>
          Deixe um comentário
        </Text>

        <Input 
          placeholder='Escreva um feedback sobre o parceiro
          (opcional)'
          multiline 
          value={value}
          onChangeText={setValue}
          h={160}
        />

        <View h={32} />

        <Button title='Avaliar' />

      </View>
    </View>
  )
}
