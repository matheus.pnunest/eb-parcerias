import { View, Text } from '@atoms'
import { LocationSelector } from '@organisms'
import { Main } from '@templates'
import { useLocationPermission, useNavigation } from '@hooks'
import { useEffect } from 'react'

export const PickLocation = () => {
  const navigation = useNavigation()
  const { location } = useLocationPermission()

  useEffect(() => {
    if (location) {
      navigation.navigate('Root')
    }
  }, [location, navigation])
  
  return (
    <Main view noPadding>
      <View ph={24} mb={24} pt={24}>
        <Text weight={400}>
          Para visualizar as parcerias da sua região,{'\n'}selecione a sua localização:
        </Text>
        <View mv={24} />
        <LocationSelector />
      </View>
    </Main>
  )
}
