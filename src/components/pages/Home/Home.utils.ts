export const segmentosList = [
  { title: 'Educação', image: require('@assets/images/22.png') },
  { title: 'Saúde', image: require('@assets/images/23.png') },
  { title: 'Serviços', image: require('@assets/images/24.png') },
]

export const parceriasList = [
  { 
    title: 'CNA',
    image: require('@assets/images/13.png'),
    segment: 'Educação',
    benefit: 'Graduação Benefício de 25%',
    expiration: '25/12/2023',
    address: 'Av. das Araucárias - Águas Claras, Brasília - DF, 70297-400'
  },
  { 
    title: 'Drogasil',
    image: require('@assets/images/14.png'),
    segment: 'Educação',
    benefit: 'Graduação Benefício de 25%',
    expiration: '25/12/2023',
    address: 'QE 13 - Conjunto D - Casa 2, s/n, Guará II, Brasília - DF, 71050-040'
  },
  { 
    title: 'Estácio',
    image: require('@assets/images/15.png'),
    segment: 'Educação',
    benefit: 'Graduação Benefício de 25%',
    expiration: '25/12/2023',
    address: 'CSG 09, Lotes 11/12/15/16, Taguatinga Sul - Taguatinga, Brasília - DF, 72035-509'
  },
]

