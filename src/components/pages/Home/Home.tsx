import { LocationTitle } from '@molecules'
import { HomeList } from '@organisms'
import { Main } from '@templates'
import { segmentosList, parceriasList } from './Home.utils'

export const Home = () => (
  <Main>
    <LocationTitle />
    <HomeList 
      title='Segmentos' 
      navigate='Segmentos'
      list={segmentosList} 
    />
    <HomeList 
      title='Parcerias' 
      navigate='Parcerias'
      list={parceriasList} 
    />
  </Main>
)
