import { AvalieOParceiro } from './AvalieOParceiro/AvalieOParceiro'
import { Avisos } from './Avisos/Avisos'
import { Cadastro } from './Cadastro/Cadastro'
import { EditarServico } from './EditarServico/EditarServico'
import { Home } from './Home/Home'
import { LinksUteis } from './LinksUteis/LinksUteis'
import { LocalizacaoAtual } from './LocalizacaoAtual/LocalizacaoAtual'
import { Mapa } from './Mapa/Mapa'
import { Menu } from './Menu/Menu'
import { Parceiro } from './Parceiro/Parceiro'
import { Parcerias } from './Parcerias/Parcerias'
import { ParceriasDetail } from './ParceriasDetail/ParceriasDetail'
import { PickLocation } from './PickLocation/PickLocation'
import { RecuperarSenha } from './RecuperarSenha/RecuperarSenha'
import { RecuperarSenha2 } from './RecuperarSenha2/RecuperarSenha2'
import { Segmentos } from './Segmentos/Segmentos'
import { SolicitacaoEncaminhada } from './SolicitacaoEncaminhada/SolicitacaoEncaminhada'

export { 
  AvalieOParceiro,
  Avisos,
  Cadastro,
  EditarServico,
  Home,
  LinksUteis,
  LocalizacaoAtual,
  Mapa,
  Menu,
  Parceiro,
  Parcerias,
  ParceriasDetail,
  PickLocation,
  RecuperarSenha,
  RecuperarSenha2,
  Segmentos,
  SolicitacaoEncaminhada,
}
