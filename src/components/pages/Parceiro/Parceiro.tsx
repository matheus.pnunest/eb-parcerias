import { useState } from 'react'
import { View, Text, Touchable } from '@atoms'
import { Input, Button } from '@molecules'
import { Main } from '@templates'
import { useNavigation } from '@hooks'

export const Parceiro = () => {
  const [cnpj, setCNPJ] = useState('')
  const [password, setPassword] = useState('')
  const navigation = useNavigation()
  return (
    <Main view>
      <Text size={24}>Login</Text>
      <View mv={8} />
      <Input 
        placeholder='CNPJ' 
        value={cnpj}
        onChangeText={setCNPJ}
      />
      <View mv={8} />
      <Input 
        placeholder='Senha' 
        value={password}
        onChangeText={setPassword}
        isPassword
      />

      <View 
        h='100%'
        main='flex-end'
        cross='center'
        pb={256}
      >
        <Button
          title='Entrar'
          onPress={() => {}}
        />
        <View mv={8} />
        <Touchable onPress={() => null}>
          <Text color='green' size={16}>Esqueceu a senha</Text>
        </Touchable>
        <View mv={8} />
        <View row>
          <Text>Não tem uma conta?</Text>
          <Touchable onPress={() => navigation.navigate('Cadastro')}>
            <Text 
              color='green' 
              ml={4}
              style={{ textDecorationLine: 'underline' }}
            >
              Cadastre-se aqui
            </Text>
          </Touchable>
        </View>
      </View>
    </Main>
  )
}
