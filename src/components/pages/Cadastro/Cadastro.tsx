import { View } from '@atoms'
import { LocationTitle, BackTitle, Input } from '@molecules'
import { Main } from '@templates'
import { CadastroProps } from './Cadastro.types'

export const Cadastro: React.FC<CadastroProps> = () => {

  return (
    <Main>
      <LocationTitle />
      <View mv={8} />
      <BackTitle title='Cadastro de Parceiro' />
      <View mv={8} />
      <Input 
        label='CNPJ'
        placeholder='Informe o CNPJ'
        small
      />
      <View mv={8} />
      <Input 
        label='Razão Social'
        placeholder='Informe a Razão Social'
        small
      />
      <View mv={8} />
      <Input 
        label='E-mail'
        placeholder='Informe o E-mail'
        small
      />
      <View mv={8} />
      <Input 
        label='Telefone'
        placeholder='()'
        small
      />
      <View mv={8} />
      <Input 
        label='Celular'
        placeholder='()'
        small
      />
  
    </Main>
  )
}
