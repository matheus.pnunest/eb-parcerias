import { MenuList } from '@organisms'
import { Main } from '@templates'

export const Menu = () => (
  <Main view>
    <MenuList />
  </Main>
)
