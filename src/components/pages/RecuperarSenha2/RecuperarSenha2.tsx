import { useState } from 'react'
import { View, Text, Icon } from '@atoms'
import { Input, Button } from '@molecules'
import { Main } from '@templates'
import { useNavigation } from '@hooks'

export const RecuperarSenha2 = () => {
  const [cnpj, setCNPJ] = useState('')
  const navigation = useNavigation()
  return (
    <Main>
      <View cross='center'>
        <View mv={24} />

        <Text size={16}>Recuperação de Senha</Text>

        <View mv={20} />

        <Text 
          size={14} 
          weight={600} 
          center 
        >
          E-mail enviado com sucesso para:
        </Text>

        <View mv={12} />

        <Input 
          label='E-mail'
          placeholder='ful***@gmail.com' 
          small
          value={cnpj}
          onChangeText={setCNPJ}
        />

        <View mv={20} />

        <Icon name='bigCheck' color='green' size={56} />

        <View mv={24} />

        <Button
          title='Entrar'
          onPress={() => {}}
          w={178}
        />
      </View>
    </Main>
  )
}
