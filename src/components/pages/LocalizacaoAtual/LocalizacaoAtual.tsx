import { View, Text } from '@atoms'
import { LocationTitle, BackTitle } from '@molecules'
import { Main } from '@templates'
import { LocalizacaoAtualProps } from './LocalizacaoAtual.types'

export const LocalizacaoAtual: React.FC<LocalizacaoAtualProps> = () => (
  <Main>
    <LocationTitle />
    <View mv={8} />
    <BackTitle title='LocalizacaoAtual' />
    <View mv={8} />
    <Text>asdaf</Text>
  </Main>
)
