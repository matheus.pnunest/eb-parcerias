import MapView from 'react-native-maps'
import { View } from '@atoms'
import { LocationTitle, BackTitle } from '@molecules'
import { Main } from '@templates'
import { MapaProps } from './Mapa.typs'
import { useLocationPermission } from '@hooks'

export const Mapa: React.FC<MapaProps> = () => {
  const { location } = useLocationPermission()

  let initialRegion = {
    latitude: -15.7942,
    longitude: -47.8822,
    latitudeDelta: 0.0922,
    longitudeDelta: 0.0421,
  }

  if (location?.coords) {
    initialRegion = {
      ...initialRegion,
      latitude: location.coords.latitude,
      longitude: location.coords.longitude,
    }
  }

  return (
    <Main view noPadding>
      <View ph={24}>
        <LocationTitle />
        <View mv={8} />
        <BackTitle title='Mapas' />
      </View>
      <View mv={8} />
      <View flex1>
        {location?.coords && (
          <MapView
            style={{ flex: 1 }}
            initialRegion={initialRegion}
          />
        )}
      </View>
    </Main>
  )
}
