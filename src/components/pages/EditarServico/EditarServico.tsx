import { View, Text, Scroll } from '@atoms'
import { CheckboxTitle } from '@molecules'
import { Main } from '@templates'

export const EditarServico = () => {
  return (
    <Main>
      <Text size={24}>Editar Serviço</Text>
      <View mv={8} />
      <Text size={12} weight={400}>
        Termos e Condições
      </Text>

      <View mv={4} />

      <Scroll p={8} bgc='gray0' h={300}>
        <Text size={16} color='gray4'>
          Labore sunt veniam amet est. Minim nisi dolor eu ad incididunt cillum elit ex ut. Dolore exercitation nulla tempor consequat aliquip occaecat. Nisi id ipsum irure aute. Deserunt sit aute irure quis nulla eu consequat fugiat Lorem sunt magna et consequat labore. Laboris incididunt id Lorem est duis deserunt nisi dolore eiusmod culpa exercitation consectetur.
          Labore sunt veniam amet est. Minim nisi dolor eu ad incididunt cillum elit ex ut. Dolore exercitation nulla tempor consequat aliquip occaecat.
          Labore sunt veniam amet est. Minim nisi dolor eu ad incididunt cillum elit ex ut. Dolore exercitation nulla tempor consequat aliquip occaecat. Nisi id ipsum irure aute. Deserunt sit aute irure quis nulla eu consequat fugiat Lorem sunt magna et consequat labore. Laboris incididunt id Lorem est duis deserunt nisi dolore eiusmod culpa exercitation consectetur.
          Labore sunt veniam amet est. Minim nisi dolor eu ad incididunt cillum elit ex ut. Dolore exercitation nulla tempor consequat aliquip occaecat.
        </Text>
      </Scroll>

      <View mv={8} />

      <CheckboxTitle title='Li os Termos do Acordo de Parceria' />

      <View mv={8} />

      <CheckboxTitle title='Concordo com os Termos do Acordo de Parceria' />

    </Main>
  )
}
