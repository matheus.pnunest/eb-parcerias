import { View } from '@atoms'
import { LocationTitle, BackTitle, HomeListCard } from '@molecules'
import { Main } from '@templates'
import { ParceriasProps } from './Parcerias.types'

export const parceriasList = [
  { 
    title: 'CNA',
    image: require('@assets/images/13.png'),
    segment: 'Educação',
    benefit: 'Graduação Benefício de 25%',
    expiration: '25/12/2023',
    address: 'Av. das Araucárias - Águas Claras, Brasília - DF, 70297-400',
    rating: '5'
  },
  { 
    title: 'Drogasil',
    image: require('@assets/images/14.png'),
    segment: 'Educação',
    benefit: 'Graduação Benefício de 25%',
    expiration: '25/12/2023',
    address: 'QE 13 - Conjunto D - Casa 2, s/n, Guará II, Brasília - DF, 71050-040',
    rating: '4,8'
  },
  { 
    title: 'Estácio',
    image: require('@assets/images/15.png'),
    segment: 'Educação',
    benefit: 'Graduação Benefício de 25%',
    expiration: '25/12/2023',
    address: 'CSG 09, Lotes 11/12/15/16, Taguatinga Sul - Taguatinga, Brasília - DF, 72035-509',
    rating: '3,8'
  },
]


export const Parcerias: React.FC<ParceriasProps> = () => (
  <Main>
    <LocationTitle />
    <View mv={8} />
    <BackTitle title='Parcerias' />
    <View mv={8} />

    {parceriasList.map(({ title, image, segment, benefit, expiration, address, rating }, idx) => (
      <HomeListCard
        key={idx}
        title={title}
        image={image}
        segment={segment}
        benefit={benefit}
        expiration={expiration}
        address={address}
        rating={rating}
        horizontal
        last={idx === parceriasList.length - 1}
      />
    ))}

  </Main>
)
