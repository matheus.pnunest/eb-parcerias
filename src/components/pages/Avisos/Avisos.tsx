import { useEffect } from 'react'
import { AvisosList } from '@organisms'
import { Main } from '@templates'
import { useNotifications } from '@hooks'

export const Avisos = () => {
  const { notifications, clearNotifications } = useNotifications()

  useEffect(() => {
    clearNotifications()
  }, [])

  return (
    <Main view>
      {notifications.length > 0 && (
        <AvisosList notifications={notifications} />
      )}
    </Main>
  )
}
