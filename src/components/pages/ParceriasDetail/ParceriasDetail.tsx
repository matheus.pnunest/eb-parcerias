import { View, Text, Touchable, Icon, Scroll } from '@atoms'
import { ParceriasDetailImage } from '@organisms'
import { useNavigation } from '@hooks'

export const ParceriasDetail = () => {
  const navigation = useNavigation()
  return (
    <View flex1>
      <ParceriasDetailImage />
      <Scroll ph={28} pt={24} hideIndicator>
        <Text>Anhanguera</Text>
        <View mv={4} />
        <Text>Educação</Text>
  
        <View mt={4} mb={20} row main='space-between' cross='center'>
          <Text size={12} width={230}>Av. das Araucárias - Águas Claras, Brasília - DF, 70297-400</Text>
          <Touchable onPress={() => null}>
            <Text color='blue'>Ver no mapa</Text>
          </Touchable>
        </View>
  
        <Text color='red'>Graduação Benefício de 25%</Text>
        <Text color='red'>Validade: 25/12/2023</Text>
  
        <View mv={24} />
  
        <View cross='center'>
          <Text weight={400}>Avalie o Parceiro</Text>
          <View mv={4} />
  
          <Touchable row onPress={() => navigation.navigate('AvalieOParceiro')}>
            {[...Array(5).keys()].map((idx) => (
              <Icon key={idx} size={32} name='star1' outlined />
            ))}
          </Touchable>
        </View>

        <View mv={200} />
      </Scroll>
    </View>
  )
}
