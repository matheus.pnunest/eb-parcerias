import { View, Text, Icon } from '@atoms'
import { Button } from '@molecules'
import { Main } from '@templates'
import { useNavigation } from '@hooks'

export const SolicitacaoEncaminhada = () => {
  const navigation = useNavigation()
  return (
    <Main>
      <View cross='center'>
        <View mv={24} />

        <Text 
          size={20} 
          weight={600} 
          center 
        >
          Sua solicitação de Divulgação foi encaminhada para Análise!
        </Text>

        <View mv={12} />

        <Icon name='bigCheck' color='green' size={56} />

        <View mv={24} />

        <Button
          title='Acompanhar'
          onPress={() => {}}
          w={248}
        />
      </View>
    </Main>
  )
}
