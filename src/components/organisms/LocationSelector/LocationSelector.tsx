import { useState } from 'react'
import { View } from '@atoms'
import { DropdownSelector } from '@molecules'
import { useStatesAndCities, useNavigation } from '@hooks'
import { LocationBottomSheet } from '../LocationBottomSheet/LocationBottomSheet'

export const LocationSelector = () => {
  const navigation = useNavigation()
  const [isVisibleEstado, setVisibleEstado] = useState(false)
  const [isVisibleCidade, setVisibleCidade] = useState(false)
  const { 
    estados, 
    cidades,
    selectedEstado, 
    setSelectedEstado, 
    selectedCidade, 
    setSelectedCidade,
    loading, 
  } = useStatesAndCities()

  const handleSelectEstado = (estadoSigla: string) => {
    setSelectedEstado(estadoSigla)
    setVisibleEstado(false)
  }

  const handleSelectCidade = (cidadeNome: string) => {
    setSelectedCidade(cidadeNome)
    setVisibleCidade(false)

    if (selectedEstado && cidadeNome) {
      navigation.navigate('Root')
    }
  }

  return (
    <>
      <DropdownSelector 
        title={selectedEstado ? selectedEstado : 'Estado'}
        isOpen={isVisibleEstado}
        onSelect={() => setVisibleEstado(!isVisibleEstado)}
      />

      <View mv={12} />

      <DropdownSelector 
        title={selectedCidade ? selectedCidade : 'Cidade'}
        isOpen={isVisibleCidade}
        onSelect={() => setVisibleCidade(!isVisibleCidade)}
        disabled={!selectedEstado}
      />

      <LocationBottomSheet
        isVisible={isVisibleEstado}
        onClose={() => setVisibleEstado(false)}
        items={estados.flatMap(estado => estado.sigla)}
        selectedValue={selectedEstado}
        handleSelect={handleSelectEstado}
        loading={loading}
      />

      <LocationBottomSheet
        isVisible={isVisibleCidade}
        onClose={() => setVisibleCidade(false)}
        items={cidades.flatMap(cidade => cidade.nome)}
        selectedValue={selectedCidade}
        handleSelect={handleSelectCidade}
        loading={loading}
      />
    </>
  )
}
