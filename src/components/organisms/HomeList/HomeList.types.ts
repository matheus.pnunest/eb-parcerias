import { ImageSourcePropType } from "react-native"
import { RoutesList } from '@src/navigation/routes/Routes.types'

export interface HomeListProps {
  title: string
  list?: {
    title: string
    image: ImageSourcePropType
  }[]
  navigate: keyof RoutesList
}
