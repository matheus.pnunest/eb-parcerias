import { View } from '@atoms'
import { HomeListCard, HomeListTitleVerTodos } from '@molecules'
import { HomeListProps } from './HomeList.types'

export const HomeList: React.FC<HomeListProps> = ({ title, list, navigate }) => (
  <View>
    <View mv={8} />
    <HomeListTitleVerTodos 
      title={title} 
      navigate={navigate}
    />

    <View row main='space-between'>
      {list?.map(({ title, image }, idx) => (
        <HomeListCard 
          key={idx} 
          title={title}
          image={image}
        />
      ))}
    </View>
  </View>
)
