import { Text, Scroll } from '@atoms'
import { BottomSheet, DropdownSelectorItem } from '@molecules'
import { LocationBottomSheetProps } from './LocationBottomSheet.types'

export const LocationBottomSheet: React.FC<LocationBottomSheetProps> = ({
  isVisible,
  onClose,
  items,
  selectedValue,
  handleSelect,
  loading,
}) => {
  return (
    <BottomSheet
      isVisible={isVisible}
      onClose={onClose}
    >
      {loading ? (
        <Text ml={24}>Loading...</Text>
      ) : (
        <Scroll hideIndicator>
          {items.map(item => (
            <DropdownSelectorItem 
              key={item} 
              title={item} 
              selected={item === selectedValue}
              onSelect={() => handleSelect(item)}
            />
          ))}
        </Scroll>
      )}
    </BottomSheet>
  )
}
