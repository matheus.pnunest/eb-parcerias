export interface LocationBottomSheetProps {
  isVisible: boolean
  onClose: () => void
  items: string[]
  selectedValue: string
  handleSelect: (value: string) => void
  loading: boolean
}
