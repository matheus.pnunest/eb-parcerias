import { RoutesList } from '@src/navigation/routes/Routes.types'

export const menuListData = [
  { 
    name: 'Segmentos', 
    icon: 'segmentos',
    navigate: 'Segmentos' as keyof RoutesList,
  },
  { 
    name: 'Parcerias', 
    icon: 'parcerias',
    navigate: 'Parcerias' as keyof RoutesList,
  },
  { 
    name: 'Mapa', 
    icon: 'mapa',
    navigate: 'Mapa' as keyof RoutesList,
  },
  { 
    name: 'Localização atual', 
    icon: 'address',
    navigate: 'LocalizacaoAtual' as keyof RoutesList,
  },
  { 
    name: 'Links Úteis', 
    icon: 'link',
    navigate: 'LinksUteis' as keyof RoutesList,
  },
]
