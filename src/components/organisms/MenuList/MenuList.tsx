import { IconType } from '@styles'
import { MenuItem } from '@molecules'
import { useNavigation } from '@hooks'
import { FlatList } from 'react-native'
import { menuListData } from './MenuList.utils'

export const MenuList = () => {
  const navigation = useNavigation()
  return (
    <FlatList
      data={menuListData}
      renderItem={({ item: { name, icon, navigate } }) => 
        <MenuItem 
          name={name} 
          icon={icon as IconType}
          onPress={() => navigation.navigate(navigate)} 
        />
      }
      keyExtractor={(item) => item.name}
    />
  )
}
