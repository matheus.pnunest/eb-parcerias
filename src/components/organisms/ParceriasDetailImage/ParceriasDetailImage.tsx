import { View, Text } from '@atoms'
import { CancelButton } from '@molecules'
import { px } from '@styles'

export const ParceriasDetailImage = () => {
  const HEIGHT = px(390)
  return (
    <View mb={HEIGHT}>
      <View 
        absolute 
        bw={4} 
        bc='red' 
        h={HEIGHT} 
        w='100%' 
        main='center' 
        cross='center'
      >
        <Text>ParceriasDetailImage</Text>
      </View>
      <CancelButton />
    </View>
  )
}
