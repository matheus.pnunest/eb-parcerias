import { FlatList } from 'react-native'
import { AvisoItem } from '@molecules'
import { AvisosListProps } from './AvisosList.types'

export const AvisosList: React.FC<AvisosListProps> = ({ notifications }) => (
  <FlatList
    data={notifications}
    renderItem={({ item: { title, date, description } }) => 
      <AvisoItem 
        title={title} 
        date={date} 
        description={description} 
      />
    }
    keyExtractor={({ id }) => id.toString()}
  />
)
