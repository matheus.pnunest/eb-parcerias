import type { NotificationList } from '@state'

export interface AvisosListProps {
  notifications: NotificationList
}
