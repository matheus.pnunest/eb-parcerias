import { useEffect } from 'react'
import { View } from '@atoms'
import { useSharedValue } from 'react-native-reanimated'
import { BottomTabItem } from '@molecules'
import { TabBarProps } from './BottomTabBar.types'
import { screenWidth } from '@styles'

export const BottomTabBar: React.FC<Partial<TabBarProps>> = ({ state, navigation, descriptors, margin = 8 }) => {
  //number of routes for calculating width
  const numOfRoutes = state?.routes.length ?? 0
  const translateShared = useSharedValue(0)

  //width of bottom tab based on margin
  const bottomTabWidth = screenWidth - margin * 2

  // width of each item = width of mover
  const itemWidth = bottomTabWidth / numOfRoutes

  useEffect(() => {
    const index = state?.index ?? 0
    translateShared.value = bottomTabWidth * (index / numOfRoutes)
  }, [state?.index, bottomTabWidth, translateShared, numOfRoutes])

  const onTabPress = (route: any, index: number) => {
    const event = navigation?.emit({
      type: 'tabPress',
      target: route.key,
      canPreventDefault: true,
    })
    const isFocused = state?.index === index
    if (!isFocused && !event?.defaultPrevented) {
      navigation?.navigate(route.name)
    }
  }

  return (
    <View 
      row 
      bgc='card' 
      bc='border'
      style={{ borderTopWidth: 1 }}
    >
      {state?.routes.map((route, index) => (
        <BottomTabItem
          key={route.key}
          route={route}
          index={index}
          state={state}
          descriptors={descriptors!}
          onTabPress={onTabPress}
          itemWidth={itemWidth}
        />
      ))}
    </View>
  )
}
