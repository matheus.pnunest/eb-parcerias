import { useEffect } from 'react'
import { useNotifications } from '@hooks'
import { Routes } from './routes/Routes'
import { NavigationContainer } from '@react-navigation/native'

export const Navigation = () => {
  const { fetchNotifications } = useNotifications()

  useEffect(() => {
    fetchNotifications()
  }, [])

  return (
    <NavigationContainer>
      <Routes />
    </NavigationContainer>
  )
}
