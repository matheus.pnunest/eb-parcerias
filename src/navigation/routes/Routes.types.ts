import { NavigatorScreenParams } from '@react-navigation/native'

export type RoutesList = {
  Root: NavigatorScreenParams<BottomTabList> | undefined
  AvalieOParceiro: undefined
  Cadastro: undefined
  EditarServico: undefined
  LinksUteis: undefined
  LocalizacaoAtual: undefined
  Mapa: undefined
  Parcerias: undefined
  ParceriasDetail: undefined
  PickLocation: undefined
  RecuperarSenha: undefined
  RecuperarSenha2: undefined
  Segmentos: undefined
  SolicitacaoEncaminhada: undefined
}

export type BottomTabList = {
  Menu: undefined
  Home: undefined
  Avisos: undefined
  Parceiro: undefined
}
