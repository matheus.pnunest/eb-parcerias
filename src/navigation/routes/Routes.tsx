import { createNativeStackNavigator } from '@react-navigation/native-stack'
import { RoutesList } from './Routes.types'
import { BottomTabNavigator } from '@containers'
import { 
  AvalieOParceiro,
  Cadastro,
  EditarServico,
  LinksUteis,
  LocalizacaoAtual,
  Mapa,
  Parcerias, 
  ParceriasDetail,
  PickLocation,
  RecuperarSenha,
  RecuperarSenha2,
  Segmentos,
  SolicitacaoEncaminhada,
} from '@pages'

const Stack = createNativeStackNavigator<RoutesList>()

export const Routes = () => (
  <Stack.Navigator 
    screenOptions={{ 
      headerShown: false, 
      animation: 'none',
    }}
    initialRouteName='PickLocation'
  >
    <Stack.Screen name='Root' component={BottomTabNavigator} />
    <Stack.Screen name='LinksUteis' component={LinksUteis} />
    <Stack.Screen name='LocalizacaoAtual' component={LocalizacaoAtual} />
    <Stack.Screen name='Mapa' component={Mapa} />
    <Stack.Screen name='Parcerias' component={Parcerias} />
    <Stack.Screen name='Segmentos' component={Segmentos} />
    <Stack.Screen name='Cadastro' component={Cadastro} />
    <Stack.Screen name='PickLocation' component={PickLocation} />
    <Stack.Screen name='RecuperarSenha' component={RecuperarSenha} />
    <Stack.Screen name='RecuperarSenha2' component={RecuperarSenha2} />
    <Stack.Screen name='SolicitacaoEncaminhada' component={SolicitacaoEncaminhada} />
    <Stack.Screen name='EditarServico' component={EditarServico} />
    <Stack.Screen 
      name='ParceriasDetail' 
      component={ParceriasDetail} 
      options={{ presentation: 'modal' }} 
    />
    <Stack.Screen 
      name='AvalieOParceiro' 
      component={AvalieOParceiro} 
      options={{ presentation: 'modal' }} 
    />
  </Stack.Navigator>
)
