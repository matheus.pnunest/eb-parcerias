import { BottomTabBar } from '@organisms'
import { BottomTabIcon } from '@molecules'
import { Menu, Home, Avisos, Parceiro } from '@pages'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { BottomTabList } from './BottomTabNavigator.types'

const Tab = createBottomTabNavigator<BottomTabList>()

export const BottomTabNavigator = () => (
  <Tab.Navigator
    tabBar={(props) => <BottomTabBar {...props} />}
    screenOptions={{ headerShown: false }}
    initialRouteName='Home'
  >
    <Tab.Screen
      options={{
        tabBarIcon: ({ focused }) => <BottomTabIcon active={focused} title='Menu' icon='menu' />
      }}
      name='Menu'
      component={Menu}
    />
    <Tab.Screen
      options={{
        tabBarIcon: ({ focused }) => <BottomTabIcon active={focused} title='Home' icon='home' />
      }}
      name='Home'
      component={Home}
    />
    <Tab.Screen
      options={{
        tabBarIcon: ({ focused }) => <BottomTabIcon active={focused} title='Avisos' icon='dorbell' />
      }}
      name='Avisos'
      component={Avisos}
    />
    <Tab.Screen
      options={{
        tabBarIcon: ({ focused }) => <BottomTabIcon active={focused} title='Parceiro' icon='handshake' />
      }}
      name='Parceiro'
      component={Parceiro}
    />
  </Tab.Navigator>
)
