import { useEffect } from 'react'
import { useRecoilState } from 'recoil'
import { notificationsState } from '@state'

export const avisosListData = [
  { id: 1, title: 'Aviso', date: '3 ago', description: 'descrição1 descrição1 descrição1 descrição1 descrição1', read: false },
  { id: 2, title: 'Aviso', date: '1 ago', description: 'descrição2 descrição2 descrição2 descrição2 descrição2', read: false },
  { id: 3, title: 'Aviso', date: '29 Jul', description: 'descrição3 descrição3 descrição3 descrição3 descrição3', read: false },
  { id: 4, title: 'Aviso', date: '2 Jun', description: 'descrição4 descrição4 descrição4 descrição4 descrição4', read: false },
]

export const useNotifications = () => {
  const [notifications, setNotifications] = useRecoilState(notificationsState)

  const fetchNotifications = () => {
    // fetch('https://api.example.com/notifications')
    //   .then(response => response.json())
    //   .then(data => setNotifications(data))
    //   .catch(error => console.error('Error fetching notifications:', error))
    
    setNotifications(avisosListData)
  }

  const clearNotifications = () => {
    const updatedNotifications = notifications.map(notification => ({
      ...notification,
      read: true,
    }))
    
    setNotifications(updatedNotifications)
  }

  return { notifications, clearNotifications, fetchNotifications }
}
