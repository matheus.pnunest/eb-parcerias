import { useState, useEffect } from 'react'
import { Estado, Cidade } from './useStatesAndCities.types'
import { useRecoilState } from 'recoil'
import { locationState } from '@state'

export const useStatesAndCities = () => {
  const [_, setLocation] = useRecoilState(locationState)
  const [estados, setEstados] = useState<Estado[]>([])
  const [cidades, setCidades] = useState<Cidade[]>([])
  const [selectedEstado, setSelectedEstado] = useState<string>('')
  const [selectedCidade, setSelectedCidade] = useState<string>('')
  const [loading, setLoading] = useState(true)

  useEffect(() => {
    fetch('https://servicodados.ibge.gov.br/api/v1/localidades/estados')
      .then(response => response.json())
      .then(data => {
        const estadosData = data
          .map((estado: { nome: string, sigla: any }) => ({
            nome: estado.nome,
            sigla: estado.sigla,
          }))
          .sort((a: { nome: string }, b: { nome: string }) => {
            if (a.nome < b.nome) return -1
            if (a.nome > b.nome) return 1
            return 0
          })
        setEstados(estadosData)
        setLoading(false)
      })
      .catch(error => console.error('Error:', error))
  }, [])

  useEffect(() => {
    if (selectedEstado) {
      setLoading(true)
      fetch(`https://servicodados.ibge.gov.br/api/v1/localidades/estados/${selectedEstado}/municipios`)
        .then(response => response.json())
        .then(data => {
          const cidadesData = data
            .map((cidade: { nome: string }) => ({ nome: cidade.nome }))
            .sort((a: { nome: string }, b: { nome: string }) => {
              if (a.nome < b.nome) return -1
              if (a.nome > b.nome) return 1
              return 0
            })
          setCidades(cidadesData)
          setLoading(false)
        })
        .catch(error => console.error('Error:', error))
    }
  }, [selectedEstado])

  const fetchCityCoordinates = async (cityName: string) => {
    try {
      const response = await fetch(`https://nominatim.openstreetmap.org/search?format=json&q=${cityName}`)
      const data = await response.json()
      if (data && data.length > 0) {
        const coordinates = {
          latitude: parseFloat(data[0].lat),
          longitude: parseFloat(data[0].lon),
        }
        return coordinates
      } else {
        throw new Error('City not found')
      }
    } catch (error) {
      console.error('Error:', error)
      throw error
    }
  }

  useEffect(() => {
    if (selectedCidade) {
      setLoading(true)
      fetchCityCoordinates(selectedCidade)
        .then(coordinates => {
          setLocation(() => ({
            coords: coordinates,
            city: selectedCidade,
            region: selectedEstado,
          }))
          setLoading(false)
        })
        .catch(error => console.error('Error:', error))
    }
  }, [selectedCidade])

  return { estados, cidades, selectedEstado, setSelectedEstado, selectedCidade, setSelectedCidade, loading }
}
