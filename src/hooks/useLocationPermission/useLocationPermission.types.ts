export interface LocationData {
  coords: {
    latitude: number
    longitude: number
  }
  timestamp: number
  city: string | null
  region: string | null
}

export interface LocationError {
  message: string
}
