import { useEffect, useState } from 'react'
import * as Location from 'expo-location'
import { LocationError } from './useLocationPermission.types'
import { useRecoilState } from 'recoil'
import { locationState } from '@state'

export const useLocationPermission = () => {
  const [location, setLocation] = useRecoilState(locationState)
  const [error, setError] = useState<LocationError | null>(null)

  const fetchLocation = async () => {
    try {
      let { status } = await Location.requestForegroundPermissionsAsync()

      if (status !== 'granted') {
        setError({ message: 'Permission to access location was denied' })
        return
      }

      let locationData = await Location.getCurrentPositionAsync({})
      
      let [geocodeData] = await Location.reverseGeocodeAsync({
        latitude: locationData.coords.latitude,
        longitude: locationData.coords.longitude,
      })

      setLocation({
        ...locationData,
        city: geocodeData.city,
        region: geocodeData.region,
      })
    } catch (error: any) {
      setError({ message: error.message })
    }
  }

  useEffect(() => {
    if (!location) {
      fetchLocation()
    }
  }, [])

  return { location, error }
}
