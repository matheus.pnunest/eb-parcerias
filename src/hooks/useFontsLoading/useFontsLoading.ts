import { useFonts } from 'expo-font'

export const useFontsLoading = () => {
  const [fontsLoaded] = useFonts({
    InterLight: require('../../assets/fonts/Inter-Light.otf'), // 300
    InterMedium: require('../../assets/fonts/Inter-Medium.otf'), // 400
    InterRegular: require('../../assets/fonts/Inter-Regular.otf'), // 500
    InterSemiBold: require('../../assets/fonts/Inter-SemiBold.otf'), // 600
  })

  if (!fontsLoaded) return false
  return fontsLoaded
}
